const chooseOptimalDistance = (t, k, ls) => {
  const findOptimalDistance = optimalDistance(ls, k);
  const res = findOptimalDistance.find((el) => {
    const sum = el.reduce((acc, next) => acc + next, 0);
    return sum < t;
  })

  return res ? res.reduce((acc, next) => acc + next, 0) : null;
};

function optimalDistance(set, k) {
  let i, j, combs, head, tailCombs;
  if (k > set.length || k <= 0) {
    return [];
  }
  if (k == set.length) {
    return [set];
  }

  if (k == 1) {
    combs = [];
    for (i = 0; i < set.length; i++) {
      combs.push([set[i]]);
    }
    return combs;
  }

  combs = [];
  for (i = 0; i < set.length - k + 1; i++) {
    head = set.slice(i, i + 1);
    tailCombs = optimalDistance(set.slice(i + 1), k - 1);
    for (j = 0; j < tailCombs.length; j++) {
      combs.push(head.concat(tailCombs[j]));
    }
  }
  return combs;
}

console.log(chooseOptimalDistance(2430, 15, [2333, 144, 132, 123, 123, 100, 89, 89, 76, 73, 68, 64, 56, 56, 50, 44, 34]));